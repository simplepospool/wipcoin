#!/bin/bash

X=$(cat /root/.wipcoin/wipcoin.conf | grep masternodeblsprivkey=)
## If no BLS key build one first!
if [ ! ${X} ]; then
  ./wipcoind -daemon
  until ./wipcoin-cli bls generate >/root/.wipcoin/bls.json 2>/dev/null; do
    echo "Waiting for daemon to start.."
    sleep 5
  done
  ./wipcoin-cli stop
  KEY=$(cat /root/.wipcoin/bls.json | grep secret | sed 's/\( "secret": "\)\|"\|,\| //g')
  echo "masternode=1" >>/root/.wipcoin/wipcoin.conf
  echo "masternodeblsprivkey=${KEY}" >>/root/.wipcoin/wipcoin.conf
fi

## Start daaemon again and be busy - if startup fails do reindex.
./wipcoind || ./wipcoind -reindex
